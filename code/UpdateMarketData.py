import DataAccess
import crypto_bot
import simulated
import datetime
import dateutil
import urllib2
import json
import imp
import ast
import os


database = "crypto.db"
currency_table = "currencies"
ticks_table = "ticks"
get_markets_currency_table = """select name, id from {0};""".format(currency_table)
get_max_tick_time = """select max(tick_time) from {0} where market_id = {1};"""
insert_ticks_table = """ INSERT INTO {0}(market_id, tick_time, data) VALUES(?,?,?) """.format(ticks_table)
get_ticks_data = """ select data from {0} where market_id = {1} and tick_time > '{2}' and tick_time < '{3}' order by tick_time"""
bittrexBaseUrl = "https://bittrex.com/Api/v2.0/pub/"

def GetTicksFromExchange(mktcode, timeInterval = 'fiveMin'):
	urlTemplate = bittrexBaseUrl + "market/GetTicks?marketName={0}&tickInterval={1}&_=1499127220008"
	url = urlTemplate.format(mktcode, timeInterval)
	raw_data = urllib2.urlopen(url).read()
	data = json.loads(raw_data)["result"]
	return data

def GetMarketTicksInTimeRange(market_id, start=None, end=None):
	if not start:
		start = datetime.datetime.min.isoformat()
	if not end:
		end = datetime.datetime.max.isoformat()
	query = get_ticks_data.format(ticks_table, market_id, start, end)
	conn = DataAccess.create_connection(database)
	cur = conn.cursor()
	cur.execute(query)
	rows = cur.fetchall()
	conn.close()
	data = [ast.literal_eval(d[0]) for d in rows] 
	return data

def GetMarketsFromDB():
	conn = DataAccess.create_connection(database)
	cur = conn.cursor()
	cur.execute(get_markets_currency_table)
	markets = cur.fetchall()
	conn.close()
	return markets

def GetMaxTickTime(market_id):
	conn = DataAccess.create_connection(database)
	cur = conn.cursor()
	cur.execute(get_max_tick_time.format(ticks_table, market_id))
	data = cur.fetchall()
	time = data[0][0]
	conn.close()
	return time

def InsertTicks(ticks, market_id):
	if not ticks:
		print "No ticks data provided for ", market_id
		return
	conn = DataAccess.create_connection(database)
	cur = conn.cursor()
	most_recent_time = GetMaxTickTime(market_id)
	for data in ticks:
		tick_time = str(data['T'])
		if most_recent_time < tick_time:
			cur.execute(insert_ticks_table, (market_id, tick_time, str(data)))
	print "Prev max time: ",most_recent_time, " Current max time:",tick_time
	conn.commit()
	conn.close()
	return 

def GetTicksFromFiles():
	files = os.listdir('../testdata')
	markets = GetMarketsFromDB()
	for market, market_id in markets:
		for file in files:
			if market in file:
				ticks = json.load(open('../testdata/'+file))["result"]
				InsertTicks(ticks, market_id)

def GetTicks():
	markets = GetMarketsFromDB()
	for market, market_id in markets:
		ticks = GetTicksFromExchange(market)
		InsertTicks(ticks, market_id)
	print "Updating Data SuccessFul"

	


