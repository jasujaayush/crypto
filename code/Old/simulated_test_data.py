import crypto_bot
import threading
import urllib2
import sched, time
import json
import pandas as pd
import matplotlib.pyplot as plt
import random
import sys
import os

class Simulated_Test_Data(object):
	def __init__(self, mktcode, jump, file, column):
		self.avg = 0.0
		self.count = 0
		self.USDT = 'USDT'
		self.mktcode = mktcode
		self.api = crypto_bot.bittrex("","")
		self.purchaseLimit = 100
		self.total = 0
		self.jump = float(jump)/100
		self.frequency = 0.000001 #seconds
		self.quantity = 0
		self.data = list(pd.read_csv(file, header = None)[column])
		self.minute = 0
		self.g = []

	def getBaseline(self):
		current = self.data[0]
		quantity = self.purchaseLimit/current
		current = self.data[-1]
		profit = (current*quantity - self.purchaseLimit - current*quantity*0.0025 - self.purchaseLimit*0.0025)
		return profit

	def buy(self,market, quantity, rate): return True

	def sell(self,market, quantity, rate): return True

	def haveOpenOrders(self,market): False

	def getMarketSummary(self):	
		d = {'Last':self.data[self.minute]}
		self.minute += 1
		return d

	def TradeMarket(self,sch, maximum, base):
		if self.minute > len(self.data)-1: 
			if self.quantity > 0:
				current = self.data[-1]
				self.total += (current*self.quantity - self.purchaseLimit - current*self.quantity*0.0025 - self.purchaseLimit*0.0025)
				self.g.append(self.total)
			return

		market = self.getMarketSummary()
		current = market['Last']
		
		self.avg = (self.avg*self.count + current)/(self.count+1)
		self.count = self.count+1

		if self.haveOpenOrders(self.mktcode):
			pass
		elif self.quantity <= 0:
			if base < 0 or current < base:
				base = current
			elif current < (base*(1+self.jump)):
				pass
			elif (current < self.avg) and current >= (base*(1+self.jump)):
				purchaseQuantity = self.purchaseLimit/current
				if self.buy(market, purchaseQuantity, current):
					maximum = current
					self.quantity = purchaseQuantity
		else:
			if current >= maximum:
				maximum = current
			else:
				purchase = self.purchaseLimit/self.quantity
				sufficientDown = current < maximum*(1-self.jump)
				profit = (current*self.quantity - self.purchaseLimit - current*self.quantity*0.0025 - self.purchaseLimit*0.0025)
				if (profit > 0 and sufficientDown):
					self.total += profit
					self.purchaseLimit += profit
					print self.minute, current, purchase, " profit/loss: "+str(profit)+" total:" + str(self.total)
					self.g.append(self.total)
					base = -1
					maximum = -1
					purchase = -1
					self.quantity = 0
				else:	
					pass

		sch.enter(self.frequency, 1, self.TradeMarket, (sch,maximum,base))

	def run(self, showPlot = False):
		scheduler = sched.scheduler(time.time, time.sleep)
		scheduler.enter(self.frequency, 1, self.TradeMarket, (scheduler,-1, -1))
		scheduler.run()
	
		if showPlot:
			print "Plotting"
			plt.plot(range(len(self.g)), self.g)
			plt.ylabel('total profit')
			plt.xlabel('Transactions')
			plt.title(self.mktcode)
			plt.show()

		return self.g[-1] if len(self.g) > 0 else 0

path = "../data/test/"
markets = [file for file in os.listdir(path) if file.endswith('.csv')]
showPlot = False
jmax = 11
for file in markets:
	output_file = open(path+'results/result_'+file,'wb')
	line = ["ColumnNum", "Baseline"] + ["Jump_" + str(i) for i in range(1,jmax, 2)]
	line = ",".join([w.ljust(20) for w in line]) + '\n'
	output_file.write(line)

	columns = pd.read_csv(path+file, header = None).columns
	for column in columns:
		sim = Simulated_Test_Data(path+file, 1, path+file, column)
		t1 = sim.getBaseline()
		line = [str(column), str(t1)]
		for jump in range(1,jmax, 2):
			sim = Simulated_Test_Data(path+file, jump, path+file, column)
			t2 = sim.run(showPlot)
			line.append(str(t2))
		output_file.write(",".join([w.ljust(15) for w in line]) + '\n')
	output_file.close()