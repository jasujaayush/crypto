import Indicators as ind
import crypto_bot
import threading
import urllib2
import sched, time
import json
import matplotlib.pyplot as plt
import random
import sys

close = 'C'
opening = 'O'
high = 'H'
low = 'L'
volume = 'V'
USDT = 'USDT'
jumpStrategy = 'jump'
movingAvgStrategy = 'ma'
buyBelowAvgStrategy = 'bba'
volumeUpStrategy = 'volume'
closeBelowOpenStrategy = 'cbo'
onlyProftSellStrategy = 'ops'
stopLossSellStrategy = 'stop'
hammerStrategy = 'hammer'
hangingManStrategy = 'hanging'
bullEngulfStrategy = 'be'
MorningStarStrategy = 'ms'
nearMinima = 'minima'

hammerWindow = 11
hangingWindow = 11
bullEngulfWindow = 11
MorningStarWindow = 11
closeToMinimaWindow = 288

class Simulated(object):
	def __init__(self, mktcode, jump, data, buystrategy = ['jump'], sellstrategy = ['jump'], fastWindow = 5, slowWindow=10):
		self.fastWindow = fastWindow
		self.slowWindow = slowWindow
		self.buystrategy = buystrategy
		self.sellstrategy = sellstrategy
		self.avg = 0.0
		self.count = 0
		self.mktcode = mktcode
		self.api = crypto_bot.bittrex("","")
		self.purchaseLimit = 100
		self.total = 0
		self.jump = float(jump)/100
		self.frequency = 0.000001 #seconds
		self.quantity = 0
		self.data = data #json.load(open(file))["result"]
		self.minute = 0
		self.g = []

	def buy(self,market, quantity, rate): return True

	def sell(self,market, quantity, rate): return True

	def haveOpenOrders(self,market): False

	def getBaseline(self):
		current = self.data[0][close]
		quantity = self.purchaseLimit/current
		current = self.data[-1][close]
		profit = (current*quantity - self.purchaseLimit - current*quantity*0.0025 - self.purchaseLimit*0.0025)
		return profit

	def shouldBuy(self, market, minima):
		close_price = market[close]
		open_price = market[opening]
		check = True
		if buyBelowAvgStrategy in self.buystrategy:
			check = check and (close_price < self.avg) 

		if jumpStrategy in self.buystrategy:
			check = check and (close_price >= (minima*(1+self.jump))) and (close_price > open_price)

		if volumeUpStrategy in self.buystrategy:
			check = check and self.isVolumeUp()

		if (movingAvgStrategy in self.buystrategy):
			start = self.minute - 2*self.slowWindow
			data = [d[close] for d in self.data[start:self.minute]]
			sema = ind.ema(data, self.slowWindow)
			fema = ind.ema(data, self.fastWindow)
			check = check and sema and fema and (sema < fema)

		if hammerStrategy in self.buystrategy:
			start = self.minute - hammerWindow
			check = check and ind.hammer(self.data[start:self.minute], hammerWindow)

		if bullEngulfStrategy in self.buystrategy:
			start = self.minute - bullEngulfWindow
			check = check and ind.bullEngulf(self.data[start:self.minute], bullEngulfWindow)			

		if MorningStarStrategy in self.buystrategy:
			start = self.minute - MorningStarWindow
			check = check and ind.MorningStar(self.data[start:self.minute], MorningStarWindow)			

		if nearMinima in self.buystrategy:
			start = self.minute - closeToMinimaWindow
			check = check and ind.closeToMinima(self.data[start:self.minute],close_price, closeToMinimaWindow)

		return check

	def isVolumeUp(self):
		cur_vol = self.data[self.minute][volume]
		pre_vol = self.data[self.minute - 1][volume]
		return cur_vol > pre_vol

	def shouldSell(self, maximum, market):
		close_price = market[close]
		open_price = market[opening]
		profit = self.getProfit(close_price)
		check = True
		if jumpStrategy in self.sellstrategy:
			check = check and (close_price < maximum*(1-self.jump))

		if movingAvgStrategy in self.sellstrategy:
			start = self.minute - 2*self.slowWindow
			data = [d[close] for d in self.data[start:self.minute]]
			sema = ind.ema(data, self.slowWindow)
			fema = ind.ema(data, self.fastWindow)
			#print "sema ", sema, fema
			check = check and sema and fema and (sema > fema)

		if closeBelowOpenStrategy in self.sellstrategy:
			check = check and (close_price < open_price)

		if onlyProftSellStrategy in self.sellstrategy:
			check = check and (profit > 0)			

		if stopLossSellStrategy in self.sellstrategy:
			purchase_price = self.quantity/self.purchaseLimit
			check = check and (close_price < 0.20*purchase_price)			

		if hangingManStrategy in self.sellstrategy:
			start = self.minute - hangingWindow
			check = check and ind.hangingMan(self.data[start:self.minute], hangingWindow)

		return check

	def getMarketSummary(self):	
		d = self.data[self.minute]
		return d

	def getProfit(self, close_price):
		if self.quantity > 0:
			profit = (close_price*self.quantity - self.purchaseLimit - close_price*self.quantity*0.0025 - self.purchaseLimit*0.0025)
			return profit
		return None

	def TradeMarket(self,sch, maximum, minima):
		if self.minute > len(self.data)-1: 
			profit = self.getProfit(self.data[-1][close])
			if profit is not None:
				self.total += profit
				self.g.append(self.total)
			return
			

		market = self.getMarketSummary()
		close_price = market[close] 
		
		self.avg = (self.avg*self.count + close_price)/(self.count+1)
		self.count = self.count+1

		if self.haveOpenOrders(self.mktcode):
			pass
		elif self.quantity <= 0:
			if minima < 0 or close_price < minima:
				minima = close_price
			elif self.shouldBuy(market, minima):
				purchaseQuantity = self.purchaseLimit/close_price
				if self.buy(market, purchaseQuantity, close_price):
					maximum = close_price
					self.quantity = purchaseQuantity
		else:
			if close_price >= maximum:
				maximum = close_price
			else:
				purchase = self.purchaseLimit/self.quantity
				if (self.shouldSell(maximum, market)):
					profit = self.getProfit(close_price)
					self.total += profit
					self.purchaseLimit += profit
					self.g.append(self.total)
					self.quantity = 0
					#print self.minute, close_price, purchase, " profit/loss: "+str(profit)+" total:" + str(self.total)
					minima, maximum, purchase = -1, -1, -1
				else:	
					pass

		self.minute += 1
		sch.enter(self.frequency, 1, self.TradeMarket, (sch,maximum,minima))

	def run(self, showPlot = False):
		scheduler = sched.scheduler(time.time, time.sleep)
		scheduler.enter(self.frequency, 1, self.TradeMarket, (scheduler,-1, -1))
		scheduler.run()
	
		if showPlot:
			print "Plotting"
			plt.plot(range(len(self.g)), self.g)
			plt.ylabel('total profit')
			plt.xlabel('Transactions')
			plt.title(self.mktcode)
			plt.show()

		return self.g[-1] if len(self.g) > 0 else 0

