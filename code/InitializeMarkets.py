import DataAccess
import crypto_bot
import simulated
import datetime
import dateutil
import urllib2
import imp

database = "crypto.db"
currency_table = "currencies"
ticks_table = "ticks"
create_currency_table = """CREATE TABLE IF NOT EXISTS {0} (id integer PRIMARY KEY, 
							name text NOT NULL, 
							last_updated datetime NOT NULL);""".format(currency_table)

create_ticks_table = """CREATE TABLE IF NOT EXISTS {0} (id integer PRIMARY KEY, 
						market_id integer, 
						tick_time datetime NOT NULL, 
						data text,
						foreign key (market_id) references {1}(id));""".format(ticks_table, currency_table)

CurrencyInsertTemplate = """ INSERT INTO {0}(name, last_updated) VALUES(?,?) """.format(currency_table)

def GetBaseMarkets(base = 'USDT'):
	api = crypto_bot.bittrex("","")
	markets = api.getmarkets()
	usdt_markets = []
	for market in markets:	
		if market['BaseCurrency'] == base:	
			usdt_markets.append(market['MarketName'])
	return usdt_markets

def MakeCurrencyTable():
	usdt_markets = GetBaseMarkets()
	print "Got Markets, trying to update database"
	conn = DataAccess.create_connection(database)
	create_table_sql = create_currency_table.format(currency_table)
	if conn and DataAccess.create_table(conn,create_table_sql):
		print "Currencies table creating sucessful"
		cur = conn.cursor()
		for market in usdt_markets:
			print market
			time = datetime.datetime.now().isoformat()
			cur.execute(CurrencyInsertTemplate, (market, time))
		conn.commit()
		conn.close()
	else:
		print "MakeCurrencyTable Failed"

def MakeTicksTable():
	conn = DataAccess.create_connection(database)
	if conn and DataAccess.create_table(conn,create_ticks_table):
		print "Ticks table created successfully"
	else:
		print "MakeTicksTable Failed"


	


