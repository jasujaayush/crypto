import crypto_bot
import urllib2
import simulated
import time
import datetime
import imp
import UpdateMarketData

def getBaseMarkets(base = 'USDT'):
	api = crypto_bot.bittrex("","")
	markets = api.getmarkets()
	usdt_markets = []
	for market in markets:	
		if market['BaseCurrency'] == base:	
			usdt_markets.append(market['MarketName'])
	return usdt_markets
	
def getMarketData(mktcode, timeInterval = 'fiveMin'):
	date = "_".join([str(t) for t in time.localtime()[:3]])
	filename = "-".join([date, mktcode, timeInterval])
	urlTemplate = "https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName={0}&tickInterval={1}&_=1499127220008"
	url = urlTemplate.format(mktcode, timeInterval)
	data = urllib2.urlopen(url).read()
	f = open("../data/"+filename, 'wb')
	f.write(data)
	f.close()
	return filename

def getData():
	markets = getBaseMarkets()
	for market in markets:
		filename = getMarketData(market)

def run(getNewData=False):
	imp.reload(simulated)
	timeInterval = 'fiveMin'
	markets = getBaseMarkets()
	if getNewData: 
		for market in markets:
			getMarketData(market) 

	buystrategy = [simulated.MorningStarStrategy, simulated.jumpStrategy]
	sellstrategy = [simulated.closeBelowOpenStrategy, simulated.onlyProftSellStrategy]#, simulated.jumpStrategy]
	date = "_".join([str(t) for t in time.localtime()[:3]])
	showPlot = False
	jmax = 11
	resultFileName = 'results'+'_'.join(['_buy'] + buystrategy)+'_'.join(['_sell'] + sellstrategy)
	file = open( resultFileName ,'wb')
	columns = ["MarketCode", "Baseline"] + ["Jump_" + str(i) for i in range(1,jmax, 2)]
	line = " ".join([w.ljust(15) for w in columns]) + '\n'
	file.write(line)
	for market in markets:
		filename = "-".join([date, market, timeInterval])
		sim = simulated.Simulated(market, 1, "../data/"+filename)
		t1 = sim.getBaseline()
		line = [market, str(t1)]
		for jump in range(1,jmax, 2):
			sim = simulated.Simulated(market, jump, "../data/"+filename, buystrategy, sellstrategy, 2, 5)
			t2 = sim.run(showPlot)
			line.append(str(t2))
		file.write(" ".join([w.ljust(15) for w in line]) + '\n')
	file.close()

def runUsingDB(getNewData=False):
	imp.reload(simulated)
	showPlot = False
	timeInterval = 'fiveMin'

	start = datetime.datetime(2018, 1, 1, 0, 0, 0).isoformat()
	end = datetime.datetime(2018, 5, 24, 0, 0, 0).isoformat()
	buystrategy = [simulated.nearMinima, simulated.jumpStrategy] #simulated.hammerStrategy, 
	sellstrategy = [simulated.movingAvgStrategy, simulated.onlyProftSellStrategy] #simulated.closeBelowOpenStrategy, 
															#simulated.jumpStrategy, simulated.stopLossSellStrategy
	jmax = 11

	markets = UpdateMarketData.GetMarketsFromDB()
	
	resultFileName = 'results/results'+'_'.join(['_buy'] + buystrategy)+'_'.join(['_sell'] + sellstrategy)
	file = open( resultFileName ,'wb')
	columns = ["MarketCode", "Baseline"] + ["Jump_" + str(i) for i in range(1,jmax, 2)]
	line = " ".join([w.ljust(15) for w in columns]) + '\n'
	file.write(line)
	for market, market_id in markets:
		data = UpdateMarketData.GetMarketTicksInTimeRange(market_id, start, end)
		print market, len(data)
		sim = simulated.Simulated(market, 1, data) #jump = 1 doesn't matter for baseline
		t1 = sim.getBaseline()
		line = [market, str(t1)]
		for jump in range(1,jmax, 2):
			sim = simulated.Simulated(market, jump, data, buystrategy, sellstrategy, 2, 5)
			t2 = sim.run(showPlot)
			line.append(str(t2))
		file.write(" ".join([w.ljust(15) for w in line]) + '\n')
	file.close()

'''
import imp
import get_data
import Indicators as ind
import UpdateMarketData

imp.reload(ind)
imp.reload(UpdateMarketData)
imp.reload(get_data)
get_data.runUsingDB()
'''