def sma(data, window):
    if len(data) < window:
        return None
    return sum(data[-window:]) / float(window)

def ema(data, window):
    if len(data) < 2 * window:
        return None # Data points not sufficient
    c = 2.0 / (window + 1)
    current_ema = sma(data[-window*2:-window], window)
    for value in data[-window:]:
        current_ema = (c * value) + ((1 - c) * current_ema)
    return current_ema

def isred(candle):
    return (candle['C'] < candle['O'])

def isgreen(candle):
    return (candle['C'] > candle['O'])

def isDowntrendDoji(middle, prev, last):
    mbody = abs(middle['C'] - middle['O'])
    pbody = prev['O'] - prev['C']
    lbody = last['C'] - last['O']
    isDoji = mbody < pbody and mbody < lbody
    mmax =  max(middle['C'], middle['O'])
    isDoji = isDoji and (mmax < prev['C']) and (mmax < last['O']) #Gap (https://cryptopotato.com/bitcoin-crypto-advanced-technical-analysis/#&gid=1&pid=1)
    return isDoji

def isDowntrend(data, window):
    redCandles = [d['C'] for d in data if isred(d)]
    isDown = (sorted(redCandles, reverse=True) == redCandles)  #Closing prices are progressively down in downtrend
    isDown = isDown and (len(redCandles) >= 2*window/3) #majority of window in red in downtrend
    return isDown

def hammer(data, window):
    if len(data) < window:
        return False
    lastCandle = data[-1]
    body = lastCandle['C'] - lastCandle['O']
    tail = lastCandle['O'] - lastCandle['L']
    #isHammer = isHammer and (lastCandle['C'] < data[-2]['C'])
    isHammer = isDowntrend(data, window)
    isHammer = isHammer and isgreen(lastCandle)
    isHammer = isHammer and (tail > 2*body) #Huge Tail compared to body
    return isHammer

def hangingMan(data, window):
    if len(data) < window:
        return False
    lastCandle = data[-1]
    body = lastCandle['O'] - lastCandle['C']
    tail = lastCandle['C'] - lastCandle['L']
    greenCandles = [d['C'] for d in data if d['C'] > d['O']]
    #isHanging = (lastCandle['C'] < lastCandle['O'])
    isHanging = isDowntrend(data, window)
    isHanging = isHanging and isred(lastCandle) #red chandle
    isHanging = isHanging and (tail > 2*body) #Huge Tail compared to body
    return isHanging
    
def bullEngulf(data, window):
    if len(data) < window:
        return False
    lastCandle = data[-1]
    secondLast = data[-2]
    isBull = isgreen(lastCandle)
    isBull = isBull and isred(secondLast)
    isBull = isBull and (lastCandle['O'] < secondLast['C']) and (lastCandle['C'] < secondLast['O']) #Engulf
    redCandles = [d['C'] for d in data if d['C'] < d['O']]
    isBull = isBull and isDowntrend(data, window)
    return isBull

def MorningStar(data, window):
    if len(data) < window > 3:
        return False
    lastCandle = data[-1]
    secondLast = data[-2]
    thirdLast =  data[-3]
    isStar = isred(thirdLast)
    isStar = isStar and isgreen(lastCandle) and (lastCandle['C'] > (thirdLast['O'] + thirdLast['C'])/2)
    redCandles = [d['C'] for d in data if d['C'] < d['O']]
    isStar = isStar and isDowntrend(data, window)
    isStar = isStar and isDowntrendDoji(secondLast, thirdLast, lastCandle)
    return isStar

def closeToMinima(data, currentPrice, window):
    if len(data) < window:
        return False
    lowPrices = [d['C'] for d in data]
    highPrices = [d['C'] for d in data]
    minima = min(lowPrices)
    maxima = max(highPrices)
    priceRange = maxima - minima
    isClose = (currentPrice < (minima + 0.05*priceRange)) and (priceRange >0.2*currentPrice)
    return isClose
